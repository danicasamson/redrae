$(document).ready(function() {
	$('.our-bestsellers-content .img-caption-center').mouseover(function(event) {
		$(this).addClass('hovered');
	});
	$('.our-bestsellers-content .img-caption-center').mouseleave(function(event) {
		$(this).removeClass('hovered');
	});
	$('.highlights-content .img-cont').mouseover(function(event) {
		$(this).find('p.hidden, .cart-btn').removeClass('hidden');
	});
	$('.highlights-content .img-cont').mouseleave(function(event) {
		$(this).find('p.description, p.price, .cart-btn').addClass('hidden');
	});
	$('.carousel[data-type="multi"] .item').each(function(){
		var next = $(this).next();
		if(!next.length){
			next = $(this).siblings(':first');
		}
		next.children(':first-child').clone().appendTo($(this)).hide().fadeIn('slow');

		for(var i = 0; i < 1; i++){
			next = next.next();
			if( !next.length){
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this)).hide().fadeIn('slow');
		}
	});
});