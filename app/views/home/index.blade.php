@extends('template.main')

@section('content')
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12 pt-20 pb-20">
				<div id="homepage-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#homepage-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#hompage-carouse" data-slide-to="1"></li>
						<li data-target="#hompage-carouse" data-slide-to="2"></li>
					</ol>

					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="{{URL::to('assets/images/home-carousel-img1.png')}}">
							<div class="carousel-caption">
								<h1>MEGA SALE</h1>
								<span>UP TO 70% OFF</span>
								<p class="pt-20">
									<a href="#">SHOP MEN</a>
									<a href="#">SHOP WOMEN</a>
									<a href="#">SHOP ALL</a>
								</p>
							</div>
						</div>

						<div class="item">
							<img src="{{URL::to('assets/images/home-carousel-img1.png')}}">
							<div class="carousel-caption">

							</div>
						</div>

						<div class="item">
							<img src="{{URL::to('assets/images/home-carousel-img1.png')}}">
							<div class="carousel-caption">

							</div>
						</div>
					</div>

					<a href="#homepage-carousel" class="left carousel-control" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
					<a href="#homepage-carousel" class="right carousel-control" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</div>

		<div class="row  pt-30 pb-30">
			<div class="col-sm-12 owl-carousel homepage-carousel2 pr-0 pl-0" id="owl-example">
				<div class="product-category">
					<img src="{{URL::to('assets/images/home-carousel2-img1.png')}}">
					<div class="category-title text-center">
						<span>BAGS</span>
					</div>
				</div>
				<div class="product-category">
					<img src="{{URL::to('assets/images/home-carousel2-img2.png')}}">
					<div class="category-title text-center">
						<span>FOOTWEAR</span>
					</div>
				</div>
				<div class="product-category">
					<img src="{{URL::to('assets/images/home-carousel2-img3.png')}}">
					<div class="category-title text-center">
						<span>JACKETS</span>
					</div>
				</div>
				<div class="product-category">
					<img src="{{URL::to('assets/images/home-carousel2-img2.png')}}">
					<div class="category-title text-center">
						<span>FOOTWEAR</span>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 pt-20 pb-20 collections-img">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12 vertical-img pr-10">
						<img src="{{URL::to('assets/images/home-collection-img1.png')}}">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 vertical-img pr-10">
						<img src="{{URL::to('assets/images/home-collection-img2.png')}}">
					</div>
				</div>
			</div>
		</div>

		<div class="row  pt-40">
			<div class="col-sm-12 our-bestsellers-header text-center  pl-0 pr-0">
				<div class="title"><span>TOP SELLERS</span></div>
				<hr class="hline">
			</div>
		</div>

		<div class="row pt-40">
			<div class="col-sm-12 our-bestsellers-content  pl-0 pr-0">
				<div class="row">
					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img1.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img2.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img3.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img4.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row  pt-40">
			<div class="col-sm-12 highlights-header text-center pl-0 pr-0">
				<div class="title"><span>HIGHLIGHTS</span></div>
				<hr class="hline">
			</div>
		</div>

		<div class="row pt-40">
			<div class="col-sm-12 highlights-content pl-0 pr-0">
				<div class="row">
					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img1.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img2.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img3.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img4.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
<script>
	$(document).ready(function(){
		$("#owl-example").owlCarousel({

			    // Most important owl features
			    items : 3,
			    itemsCustom : false,
			    itemsDesktop : [1199,3],
			    itemsDesktopSmall : [980,3],
			    itemsTablet: [768,2],
			    itemsTabletSmall: false,
			    itemsMobile : [479,1],
			    singleItem : false,
			    itemsScaleUp : false,

			    //Basic Speeds
			    slideSpeed : 200,
			    paginationSpeed : 800,
			    rewindSpeed : 1000,

			    //Autoplay
			    autoPlay : true,
			    stopOnHover : true,

			    // Navigation
			    navigation : true,
			    navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			    rewindNav : true,
			    scrollPerPage : false,

			    //Pagination
			    pagination : false,
			    paginationNumbers: false,

			    // Responsive
			    responsive: true,
			    responsiveRefreshRate : 200,
			    responsiveBaseWidth: window,

			    // CSS Styles
			    baseClass : "owl-carousel",
			    theme : "owl-theme",

			    //Lazy load
			    lazyLoad : true,
			    lazyFollow : true,
			    lazyEffect : "fade",

			    //Auto height
			    autoHeight : false,

			    //JSON
			    jsonPath : false,
			    jsonSuccess : false,

			    //Mouse Events
			    dragBeforeAnimFinish : true,
			    mouseDrag : true,
			    touchDrag : true,

			    //Transitions
			    transitionStyle : false,

			    // Other
			    addClassActive : false,

			    //Callbacks
			    beforeUpdate : false,
			    afterUpdate : false,
			    beforeInit: false,
			    afterInit: false,
			    beforeMove: false,
			    afterMove: false,
			    afterAction: false,
			    startDragging : false,
			    afterLazyLoad : false

			});
	});
</script>
@stop