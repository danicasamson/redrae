@extends('template.main')

@section('content')
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12 product-breadcrumb">
				HOME > FEATURED PRODUCTS > <strong>THE AIGLE LADIES CUCKMERRY JACKET</strong>
			</div>
		</div>

		<div class="row product-content pt-40">
			<div class="col-sm-4 product-images-cont">
				<div class="row">
					<div class="col-sm-12 text-center pt-20">
						<img src="{{URL::to('assets/images/single-product-img1.png')}}">
						<h4 class="pt-10"><i class="fa fa-search"></i> Larger view</h4>
					</div>
				</div>
				<div class="row  pt-30 pb-30">
					<div class="col-sm-12 owl-carousel single-product-carousel" id="owl-example">
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img1.png')}}">
						</div>
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img2.png')}}">
						</div>
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img3.png')}}">
						</div>
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img1.png')}}">
						</div>
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img2.png')}}">
						</div>
						<div class="product-images">
							<img src="{{URL::to('assets/images/single-product-gal-img3.png')}}">
						</div>
					</div>
				</div>
				<div class="row pt-15">
					<div class="col-sm-12 promo-sidebar pr-0 pl-0">
						<img src="{{URL::to('assets/images/promo-sidebar-img.png')}}">
					</div>
				</div>
			</div>

			<div class="col-sm-6 pl-30">
				<div class="row">
					<div class="col-sm-12 product-title">
						<h2 class="mt-0">AIGLE LADIES CUCKMERRY DUCK DOWN JACKET - SOURIS</h2>
						<a href="#"><strong>NEW SEASON</strong></a>
					</div>
				</div>
				<div class="row pt-40">
					<div class="col-sm-12 product-price">
						<p>WAS L260.00 - <span class="txt-brown"><strong>NOW 280.00</strong> (SAVE 20%)</span></p>
						<p class="txt-italic">Free U.K. Delivery (Price includes VAT & P + P)</p>
					</div>
				</div>
				<div class="row pt-20">
					<div class="col-sm-12">
						<p>The Aigle Ladies Cuckmerry Jacket is Aigles new Autumn/Winter season wonder !!It has everything you would want from a autumn winter coat, it has style, warmth, practicality, it is a fine example of the highest quility workmanship, it is no wonder we have been taking early orders for this lovely coat </p>
					</div>
				</div>
				<div class="row pt-30 pl-15 pr-15">
					<div class="col-sm-12 product-details pt-20 pb-20">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  							<div class="panel panel-default">
    							<div class="panel-heading" role="tab" id="headingOne">
      								<h4 class="panel-title">
        								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          									Product Description <span><i class="fa fa-plus"></i></span>
        								</a>
      								</h4>
    							</div>
    							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      								<div class="panel-body">
      									Product Description
      								</div>
    							</div>
  							</div>
  							<div class="panel panel-default">
    							<div class="panel-heading" role="tab" id="headingTwo">
      								<h4 class="panel-title">
        								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          									Details & Care
        								</a>
      								</h4>
    							</div>
    							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      								<div class="panel-body">
        								Details & Care
							      	</div>
							    </div>
							</div>
  							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingThree">
							      	<h4 class="panel-title">
							        	<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          		Delivery & Returns
							        	</a>
							      	</h4>
							    </div>
							    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							      	<div class="panel-body">
							        	Delivery & Returns
							      	</div>
							    </div>
  							</div>
  							<div class="panel panel-default">
    							<div class="panel-heading" role="tab" id="headingFour">
      								<h4 class="panel-title">
        								<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
          									Size Chart <span><i class="fa fa-plus"></i></span>
        								</a>
      								</h4>
    							</div>
    							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      								<div class="panel-body">
      									Size Chart
      								</div>
    							</div>
  							</div>
  							<div class="panel panel-default">
    							<div class="panel-heading" role="tab" id="headingFive">
      								<h4 class="panel-title">
        								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          									Price Match
        								</a>
      								</h4>
    							</div>
    							<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      								<div class="panel-body">
        								Price Match
							      	</div>
							    </div>
							</div>
						</div>
					</div>
				</div>
				<div class="row pt-30 pb-20">
					<div class="col-sm-12 product-options">
						<div class="row">
							<div class="col-sm-12 availability">
								Availability: <span>In Stock</span>
							</div>
						</div>
						<div class="row pt-20">
							<div class="col-sm-12 sizes">
								Size:
								<span class="size-option">6</span>
								<span class="size-option">8</span>
								<span class="size-option">10</span>
								<span class="size-option">12</span>
								<span class="size-option">14</span>
								<span class="size-option">16</span>
							</div>
						</div>
						<div class="row pt-20">
							<div class="col-sm-12 colours">
								Colours:
								<span class="colour-option black"></span>
								<span class="colour-option blue"></span>
								<span class="colour-option grey"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row pt-20">
					<div class="col-sm-6">
						<button class="add-to-bag">ADD TO SHOPPING BAG</button>
					</div>
					<div class="col-sm-6 text-center" style="margin-top:-5px;">
						<p><strong>Secured payment</strong></p>
						<img src="{{URL::to('assets/images/cards-icon.png')}}">
					</div>
				</div>
				<div class="row pt-40">
					<div class="col-sm-6">
						<i class="fa fa-heart"></i> ADD TO WISHLIST
					</div>
					<div class="col-sm-6">
						<i class="fa fa-share"></i> SHARE
					</div>
				</div>
			</div>
			<div class="col-sm-2 related-products">
				<div class="row">
					<div class="col-sm-12 text-center related-products-title pb-10">
						<strong>RELATED PRODUCTS</strong>
					</div>
				</div>
				<div class="row pt-30 text-center">
					<div class="col-sm-12 related-product">
						<img src="{{URL::to('assets/images/related-products-img1.png')}}">
						<p class="pt-10">Aigle</p>
						<p class="txt-grey">Oldhaven Jacket</p>
						<p class="txt-grey">L199.00</p>
					</div>
				</div>
				<div class="row pt-30 text-center">
					<div class="col-sm-12 related-product">
						<img src="{{URL::to('assets/images/related-products-img1.png')}}">
						<p class="pt-10">Aigle</p>
						<p class="txt-grey">Oldhaven Jacket</p>
						<p class="txt-grey">L199.00</p>
					</div>
				</div>
				<div class="row pt-30 text-center">
					<div class="col-sm-12 related-product">
						<img src="{{URL::to('assets/images/related-products-img1.png')}}">
						<p class="pt-10">Aigle</p>
						<p class="txt-grey">Oldhaven Jacket</p>
						<p class="txt-grey">L199.00</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row  pt-40">
			<div class="col-sm-12 highlights-header text-center pl-0 pr-0">
				<div class="title"><span>YOU MAY ALSO LIKE</span></div>
				<hr class="hline">
			</div>
		</div>

		<div class="row pt-40">
			<div class="col-sm-12 highlights-content pl-0 pr-0">
				<div class="row">
					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img1.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img2.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img3.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/highlights-img4.png')}}">
						<div class="cart-btn text-center hidden">
							<a href="#">ADD TO CART</a>
						</div>
						<div class="img-caption-bottom pt-10 text-center">
							<p class="brand"><img src="{{URL::to('assets/images/brand-icon1.png')}}"> Aigle Cuckmerry</p>
							<p class="description hidden">COUNTRY ATTIRE LADIES' KENDAL WAX PARKA - OLIVE</p>
							<p class="price hidden">&euro;259.95</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row  pt-40">
			<div class="col-sm-12 our-bestsellers-header text-center  pl-0 pr-0">
				<div class="title"><span>PEOPLE ALSO VIEWED</span></div>
				<hr class="hline">
			</div>
		</div>

		<div class="row pt-40">
			<div class="col-sm-12 our-bestsellers-content  pl-0 pr-0">
				<div class="row">
					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img1.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img2.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img3.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>

					<div class="col-md-3 col-xs-12 col-sm-3 img-cont">
						<img src="{{URL::to('assets/images/home-bestseller-img4.png')}}">
						<div class="img-caption-center pt-40 pb-30 pl-20 pr-20 text-center">
							<h5><strong>NEW AUTUMN/ WINTER 2014</strong></h5>
							<a href="#" class="btn btn-brown">Get It Now</a><br/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
	<script>
		$(document).ready(function(){
			$("#owl-example").owlCarousel({

			    // Most important owl features
			    items : 3,
			    itemsCustom : false,
			    itemsDesktop : [1199,3],
			    itemsDesktopSmall : [980,3],
			    itemsTablet: [768,2],
			    itemsTabletSmall: false,
			    itemsMobile : [479,1],
			    singleItem : false,
			    itemsScaleUp : false,

			    //Basic Speeds
			    slideSpeed : 200,
			    paginationSpeed : 800,
			    rewindSpeed : 1000,

			    //Autoplay
			    autoPlay : true,
			    stopOnHover : true,

			    // Navigation
			    navigation : true,
			    navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			    rewindNav : true,
			    scrollPerPage : false,

			    //Pagination
			    pagination : false,
			    paginationNumbers: false,

			    // Responsive
			    responsive: true,
			    responsiveRefreshRate : 200,
			    responsiveBaseWidth: window,

			    // CSS Styles
			    baseClass : "owl-carousel",
			    theme : "owl-theme",

			    //Lazy load
			    lazyLoad : true,
			    lazyFollow : true,
			    lazyEffect : "fade",

			    //Auto height
			    autoHeight : false,

			    //JSON
			    jsonPath : false,
			    jsonSuccess : false,

			    //Mouse Events
			    dragBeforeAnimFinish : true,
			    mouseDrag : true,
			    touchDrag : true,

			    //Transitions
			    transitionStyle : false,

			    // Other
			    addClassActive : false,

			    //Callbacks
			    beforeUpdate : false,
			    afterUpdate : false,
			    beforeInit: false,
			    afterInit: false,
			    beforeMove: false,
			    afterMove: false,
			    afterAction: false,
			    startDragging : false,
			    afterLazyLoad : false

			});
		});
	</script>
@stop