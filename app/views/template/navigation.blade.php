<div class="col-sm-12">
	<div class="top-menubar digimeg-nav-wrap">
		<ul class="nav nav-pills digimeg-group digimeg-main-nav">
			<li><a href="#">HOME</a></li>
			<li><a href="#" data-subnav="our-brands-dropdown">OUR BRANDS</a></li>
			<li><a href="#" data-subnav="new-arrivals-dropdown">NEW ARRIVALS</a></li>
			<li><a href="#" data-subnav="women-dropdown">WOMEN</a></li>
			<li><a href="#" data-subnav="men-dropdown">MEN</a></li>
			<li><a href="#" data-subnav="children-dropdown">CHILDREN</a></li>
			<li><a href="#" data-subnav="footwear-dropdown">FOOTWEAR</a></li>
			<li><a href="#" data-subnav="accessories-dropdown">ACCESSORIES</a></li>
			<li><a href="#" data-subnav="sale-dropdown">SALE</a></li>
		</ul>
	</div>
	<ul class="digimeg-nav-down-content digimeg-sub-nav nav">
		<li id="our-brands-dropdown" class="dropdown">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Aigle</a></li>
						<li class="subheader"><a href="#">Alpha Industries</a></li>
						<li class="subheader"><a href="#">Ashwood</a></li>
						<li class="subheader"><a href="#">Barbour</a></li>
						<li class="subheader"><a href="#">Barmah Hats</a></li>
						<li class="subheader"><a href="#">Brady</a></li>
						<li class="subheader"><a href="#">Dr. Martens</a></li>
						<li class="subheader"><a href="#">Falcon</a></li>
						<li class="subheader"><a href="#">Gamebird</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Gaudi</a></li>
						<li class="subheader"><a href="#">Gorringe</a></li>
						<li class="subheader"><a href="#">Hackett London</a></li>
						<li class="subheader"><a href="#">Hoggs of Fife</a></li>
						<li class="subheader"><a href="#">House of Cheviot</a></li>
						<li class="subheader"><a href="#">Hunter</a></li>
						<li class="subheader"><a href="#">IKKS</a></li>
						<li class="subheader"><a href="#">I - Stay</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Johnston of Elgin</a></li>
						<li class="subheader"><a href="#">Joules</a></li>
						<li class="subheader"><a href="#">Kakadu</a></li>
						<li class="subheader"><a href="#">Le Chameau</a></li>
						<li class="subheader"><a href="#">Marta Ponti</a></li>
						<li class="subheader"><a href="#">Minimum</a></li>
						<li class="subheader"><a href="#">Musto</a></li>
						<li class="subheader"><a href="#">Red Rae</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Relish</a></li>
						<li class="subheader"><a href="#">Stetson</a></li>
						<li class="subheader"><a href="#">Tilley Hats</a></li>
						<li class="subheader"><a href="#">Tony Perotti</a></li>
						<li class="subheader"><a href="#">Tumble & Hide</a></li>
						<li class="subheader"><a href="#">Visconti</a></li>
						<li class="subheader"><a href="#">Yoshi</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 our-brands-letter-pagination">
					<ul class="nav">
						<li role="presentation"><a >FEATURED</a></li>
						<li role="presentation"><a href="#">A</a></li>
						<li role="presentation"><a href="#">B</a></li>
						<li role="presentation"><a href="#">C</a></li>
						<li role="presentation"><a href="#">D</a></li>
						<li role="presentation"><a href="#">E</a></li>
						<li role="presentation"><a href="#">F</a></li>
						<li><a href="#">G</a></li>
						<li><a href="#">H</a></li>
						<li><a href="#">I</a></li>
						<li><a href="#">J</a></li>
						<li><a href="#">K</a></li>
						<li><a href="#">L</a></li>
						<li><a href="#">M</a></li>
						<li><a href="#">N</a></li>
						<li><a href="#">O</a></li>
						<li><a href="#">P</a></li>
						<li><a href="#">Q</a></li>
						<li><a href="#">R</a></li>
						<li><a href="#">S</a></li>
						<li><a href="#">T</a></li>
						<li><a href="#">U</a></li>
						<li><a href="#">V</a></li>
						<li><a href="#">W</a></li>
						<li><a href="#">X</a></li>
						<li><a href="#">Y</a></li>
						<li><a href="#">Z</a></li>
						<li><a href="#">VIEW ALL</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="new-arrivals-dropdown">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav">
						<li class="subheader"><a href="#">Mens New Arrivals</a></li>
						<li class="subheader"><a href="#">Womens New Arrivals</a></li>
						<li class="subheader"><a href="#">Childrens New Arrivals</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="women-dropdown">
			<div class="row">
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">View all Womenswear</a></li>
						<li class="subheader"><a href="#">New Arrivals</a></li>
						<li class="subheader"><a href="#">Gifts for her</a></li>
						<li class="subheader"><a href="#">Accessories</a></li>
						<li class="subheader"><a href="#">Bags</a></li>
						<li class="subheader"><a href="#">Cardigans & Jumpers</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Jackets & Coats</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">Parkas</a></li>
								<li><a href="#">Down Jackets</a></li>
								<li><a href="#">Quilted Jackets</a></li>
								<li><a href="#">Wax Jackets</a></li>
								<li><a href="#">Waterproof/Breathable Jackets</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Jeans</a></li>
						<li class="subheader"><a href="#">Midlayers</a></li>
						<li class="subheader"><a href="#">Waistcoats/Gilet</a></li>
						<li class="subheader"><a href="#">Wellingtons</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="men-dropdown">
			<div class="row">
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">View all Menswear</a></li>
						<li class="subheader"><a href="#">New Arrivals</a></li>
						<li class="subheader"><a href="#">Gifts for him</a></li>
						<li class="subheader"><a href="#">Accessories</a></li>
						<li class="subheader"><a href="#">Bags</a></li>
						<li class="subheader"><a href="#">Cardigans & Jumpers</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Jackets & Coats</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">Parkas</a></li>
								<li><a href="#">Down Jackets</a></li>
								<li><a href="#">Quilted Jackets</a></li>
								<li><a href="#">Wax Jackets</a></li>
								<li><a href="#">Waterproof/Breathable Jackets</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Jeans</a></li>
						<li class="subheader"><a href="#">Midlayers</a></li>
						<li class="subheader"><a href="#">Underwear</a></li>
						<li class="subheader"><a href="#">Waistcoats/Gilet</a></li>
						<li class="subheader"><a href="#">Wellingtons</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="children-dropdown">
			<div class="row">
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">View all Childrenswear</a></li>
						<li class="subheader"><a href="#">New Arrivals</a></li>
						<li class="subheader"><a href="#">Cardigans & Jumpers</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Jackets & Coats</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">Down Jackets</a></li>
								<li><a href="#">Quilted Jackets</a></li>
								<li><a href="#">Wax Jackets</a></li>
								<li><a href="#">Waterproof/Breathable Jackets</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Waistcoat/Gillet</a></li>
						<li class="subheader"><a href="#">Wellingtons</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="footwear-dropdown">
			<div class="row">
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Womens Footwear</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">View all Footwear</a></li>
								<li><a href="#">Boots</a></li>
								<li><a href="#">Wellingtons</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="nav">
						<li class="subheader"><a href="#">Mens Footwear</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">View all Footwear</a></li>
								<li><a href="#">Boots</a></li>
								<li><a href="#">Wellingtons</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</li>
		<li id="accessories-dropdown">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">View all Accessories</a></li>
						<li class="subheader"><a href="#">Bags</a></li>
						<li class="subheader"><a href="#">Belts</a></li>
						<li class="subheader"><a href="#">Briefcases</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Folios</a></li>
						<li class="subheader"><a href="#">Gloves</a></li>
						<li class="subheader"><a href="#">Hats & Hoods</a></li>
						<li class="subheader"><a href="#">Purses & Wallets</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Satchels</a></li>
						<li class="subheader"><a href="#">Scarves</a></li>
						<li class="subheader"><a href="#">Socks</a></li>
						<li class="subheader"><a href="#">Umbrellas</a></li>
					</ul>
				</div>
			</div>
		</li>
		<li id="sale-dropdown">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Womens Sale</a></li>
						<li class="subheader"><a href="#">View all Sale</a></li>
						<li class="subheader"><a href="#">Everything <= &euro;50</a></li>
						<li class="subheader"><a href="#">Accessories</a></li>
						<li class="subheader"><a href="#">Bags</a></li>
						<li class="subheader"><a href="#">Cardigans & Jumpers</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Jackets & Coats</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">Down Jackets</a></li>
								<li><a href="#">Quilted Jackets</a></li>
								<li><a href="#">Wax Jackets</a></li>
								<li><a href="#">Waterproof/Breathable Jackets</a></li>
							</ul>
						</li>
						<li class="subheader"><a href="#">Jeans</a></li>
						<li class="subheader"><a href="#">Midlayers</a></li>
						<li class="subheader"><a href="#">Waistcoat/Gilet</a></li>
						<li class="subheader"><a href="#">Wellingtons</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Mens Sale</a></li>
						<li class="subheader"><a href="#">View all Sale</a></li>
						<li class="subheader"><a href="#">Everything <= &euro;50</a></li>
						<li class="subheader"><a href="#">Accessories</a></li>
						<li class="subheader"><a href="#">Bags</a></li>
						<li class="subheader"><a href="#">Cardigans & Jumpers</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav">
						<li class="subheader"><a href="#">Jackets & Coats</a></li>
						<li>
							<ul class="nav">
								<li><a href="#">Down Jackets</a></li>
								<li><a href="#">Quilted Jackets</a></li>
								<li><a href="#">Wax Jackets</a></li>
								<li><a href="#">Waterproof/Breathable Jackets</a></li>
							</ul>
						</li>
						<li class="subheader"><a href="#">Jeans</a></li>
						<li class="subheader"><a href="#">Midlayers</a></li>
						<li class="subheader"><a href="#">Waistcoat/Gilet</a></li>
						<li class="subheader"><a href="#">Wellingtons</a></li>
					</ul>
				</div>
			</div>
		</li>
	</ul>
</div>