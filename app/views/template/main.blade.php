<!DOCTYPE html>
<html>
    <head>

        @yield('metatags')

        <meta name="copyright" content="" />
        <meta name="email" content="" />
        <meta name="Distribution" content="Global" />
        <meta name="Rating" content="General" />
        <meta name="robots" content="index,follow">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="shortcut icon" href="">
        <link rel="author" href=""/>

        {{ HTML::style('assets/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/custom.css') }}
        {{ HTML::style('assets/css/owl.carousel.css') }}
        {{ HTML::style('assets/css/owl.theme.css') }}
        {{ HTML::style('assets/css/digimeg.css') }}
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <style type="text/css">
          @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800);
          body{font-size: 15px;color: #000;}
        </style>

        @yield('style')
        <script>
            var base_url = "{{ URL::to('/') }}";
        </script>

    </head>

    <body>
        <div class="container">
            <div class="row">
                @include('template.header')
            </div>
            <div class="row">
                @yield('content')
            </div>
            <div class="row">
                @include('template.footer')
            </div>
        </div>

        @yield('script')

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
        {{ HTML::script('assets/js/bootstrap.min.js') }}
        {{ HTML::script('assets/js/custom.js') }}
        {{ HTML::script('assets/js/jquery.min.js') }}
        {{ HTML::script('assets/js/owl.carousel.js') }}
        {{ HTML::script('assets/js/owl.carousel.min.js') }}
        {{ HTML::script('assets/js/digimeg.js') }}
    </body>
</html>