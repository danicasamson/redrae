<div class="col-sm-12 pt-40 footer">
	<div class="row">
		<div class="col-sm-12 header text-center  pl-0 pr-0">
			<hr class="hline">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 content pt-40  pl-0 pr-0">
			<div class="row">
				<div class="col-sm-3">
					<p><strong>LOOKING FOR US</strong></p>
					<p>Red Rae Town & Country</p>
					<p>25 Amwell End</p>
					<p>Ware</p>
					<p>Herts</p>
					<p>SG12 9HP</p>
					<p>(UK) 01920 463170</p>
					<p>sales&#64;redrae.co.uk</p>
				</div>

				<div class="col-sm-3">
					<p><strong>CUSTOMER SERVICES</strong></p>
					<p>Contact Us</p>
					<p>Returns</p>
					<p>Worldwide Delivery</p>
					<p>Pay with Paypal</p>
					<p>Safe &amp; Secure</p>
					<p>Feedback</p>
					<p>Price Match</p>
				</div>

				<div class="col-sm-3">
					<p><strong>WHATS IN STORE</strong></p>
					<p>Our Brands</p>
					<p>Special Offers</p>
					<p>This Months Special</p>
					<p>Sale</p>
				</div>

				<div class="col-sm-3">
					<p><strong>HOW CAN WE HELP?</strong></p>
					<p>Customer Service</p>
					<p>About Us</p>
					<p>Terms &amp; Conditions</p>
					<p>Sponsored Links</p>
					<p>Orderding FAQ</p>
					<p>Repairs</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 pt-40 pt-40 text-center subscribe-to-newsletter form-inline  pl-0 pr-0">
			<div class="form-group">
				<label for="subscribers_email"><i class="fa fa-envelope"></i> SIGN UP FOR THE LATEST NEWS, OFFERS AND IDEAS: </label>
			</div>
			<div class="form-group">
				<input type="text" name="subscribers_email" placeholder="Enter your email address" class="form-control">
			</div>
			<a href="#">SUBMIT</a>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 pt-40 pb-40  pl-0 pr-0">
			<div class="row">
				<div class="col-sm-6">
					<p><strong>CONNECT WITH US</strong></p>
					<img src="{{URL::to('assets/images/fb-icon.png')}}">
					<img src="{{URL::to('assets/images/instagram-icon.png')}}">
					<img src="{{URL::to('assets/images/pinterest-icon.png')}}">
					<img src="{{URL::to('assets/images/twitter-icon.png')}}">
				</div>
				<div class="col-sm-3 text-right">
					<img src="{{URL::to('assets/images/verisign-icon.png')}}">
					<img src="{{URL::to('assets/images/paypal-icon.png')}}" style="margin-left:10px">
				</div>
				<div class="col-sm-3">
					<p><strong>PAYMENT THROUGH</strong></p>
					<img src="{{URL::to('assets/images/cards-icon.png')}}">
				</div>
			</div>
		</div>
	</div>
</div>