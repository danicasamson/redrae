<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-12  pt-10 pb-10 pr-10">
							<div class="row">
								<div class="col-md-6 col-sm-3 col-xs-5 searchbar-col">
									<div class="form-group  has-feedback">
										<input type="text" class="form-control" placeholder="Search items here">
										<span class="glyphicon glyphicon-search form-control-feedback" aria-hidden-true></span>
									</div>
								</div>
								<div class="col-md-4 col-sm-3 col-xs-5 select-language">
									<select class="form-control">
										<option>Select language</option>
										<option>English</option>
										<option>Spanish</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-12 auth-row pt-10 pb-10 pr-10">
							<ul class="nav nav-pills pull-right">
								<li class="divider"><a href="#">Login</a></li>
								<li class="divider"><a href="#">Register</a></li>
								<li><a href="#">Shopping Bag <span class="shopping-bag-icon">0</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row pb-20">
		<div class="col-sm-12 logo-row text-center">
			<img src="{{URL::to('assets/images/logo.png')}}">
		</div>
	</div>

	<div class="row">

	</div>

	<div class="row">
		@include('template.navigation')
	</div>

	<div class="row">
		<div class="col-sm-12 benefits-bar pt-20 pb-20">
			<ul class="nav nav-pills nav-justified text-center">
				<li>FREE UK & <br/>WORLDWIDE DELIVERY</li>
				<li>SIGN UP TO OUR NEWSLETTER & <br/>RECEIVE 10% OFF WHEN YOU NEXT ORDER</li>
				<li>20% OFF YOUR WINTER ORDER <br/>QUOTE WINTERORDER20 IN BASKET</li>
				<li class="last-li">CALL US 09120 463170</li>
			</ul>
		</div>
	</div>
</div>